const express = require('express')
const router = express.Router()
const addNotes = require('src/models/add-notes')
const lookup = require('src/models/lookup')

// set up environment constants
const urlBase = process.env.sugarcrm_url


function getHex() {
  const possible = "0123456789ABCDEF"
  return possible.charAt(Math.floor(Math.random() * possible.length))
}

function getConfirmationNumber() {
  let id = ''
  for (let i = 0; i < 10; i++) {
    id += getHex()
  }
  return id
}

router.post('/:ani', async function (req, res, next) {
  const ani = req.params.ani
  try {
    // generate random ID
    const confirmation = getConfirmationNumber(ani, req.body)
    // perform lookup
    const url = `${urlBase}/rest/v10/Contacts`
    const members = await lookup(url, 'phone_work', ani)
    console.log('found ${members.length} Contacts:')
    // write to sugarCRM database for customer
    // extract customer ID
    const id = members[0].id
    console.log('choosing first Contact ID: ' + id)
    // add confirmation number to member's notes
    try {
      const name = 'payment confirmation - ' + confirmation
      const description = 'customer paid $300'
      // add note using SugarCRM API
      // console.log('adding note to Contact' + id)
      await addNotes(id, name, description)
    } catch (e1) {
      console.log(e1)
      // just continue
    }
    // success
    res.status(202).send({confirmation})
  } catch (e) {
    console.log(e)
    res.status(500).send('Failed to add note')
  }
})

module.exports = router
