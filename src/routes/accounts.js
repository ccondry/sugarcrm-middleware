const express = require('express')
const router = express.Router()
const getPage = require('src/models/get-page')

router.get('/', getPage('Accounts'))

module.exports = router
