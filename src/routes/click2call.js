const express = require('express')
const router = express.Router()
const request = require('request')

router.get('/:mediaAddress', async function (req, res, next) {
  const mediaAddress = req.params.mediaAddress
  // run agent request API
  const options = {
    url: process.env.socialminer_base_url + '/ccp/callback/feed/100020',
    qs: {
      title: 'None',
      name: 'None',
      description: 'None',
      mediaAddress,
      'variable_user_user.id': process.env.click2call_userid
    }
  }
  try {
    const response = request(options)
    console.log('agent request API request sent')
    // console.log(response)
    res.status(200).send('call started')
  } catch (e) {
    console.log('agent request API request failed', e)
    res.status(500).send()
  }
})

router.post('/*', function (req, res, next) {
  console.log(`click2call request path`, req.path)
  console.log(`click2call request query`, req.query)
  console.log(`click2call request body`, req.body)
})

module.exports = router
