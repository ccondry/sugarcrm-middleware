const express = require('express')
const router = express.Router()
const pkg = require('../../package.json')

// say version
router.get('/', function (req, res, next) {
  res
  .set("Content-type", "application/json; charset=utf-8")
  .send({version: pkg.version})
})

module.exports = router
