const axios = require('axios')

// set up environment constants
const urlBase = process.env.sugarcrm_url
const creds = {
  grant_type: process.env.sugarcrm_grant_type,
  client_id: process.env.sugarcrm_client_id,
  client_secret: process.env.sugarcrm_client_secret,
  username: process.env.sugarcrm_username,
  password: process.env.sugarcrm_password,
  platform: process.env.sugarcrm_platform
}

module.exports = {
  getToken: async function () {
    console.log('getting OAuth2 token')
    const url = `${urlBase}/rest/v10/oauth2/token`
    const oauthResponse = await axios.post(url, creds)
    const accessToken = oauthResponse.data.access_token
    console.log('got OAuth2 token: ' + accessToken)
    return accessToken
  }
}
