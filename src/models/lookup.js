const getOauth2Token = require('src/models/oauth').getToken
const axios = require('axios')

module.exports = async function (url, field, value) {
  console.log(`looking up ${url} with ${field} = ${value}`)
  // get an OAuth2 access token for the lookup request
  const accessToken = await getOauth2Token()
  // construct customer lookup query
  const query = {}
  query[field] = value
  // strigify the JSON query object and use it as a GET URL parameter
  const params = {
    filter: JSON.stringify([query])
  }
  // set OAuth2 token header
  const headers = {
    'oauth-token': accessToken
  }
  // go get the data
  try {
    const results = await axios.get(url, {params, headers})
    console.log(`lookup done. found ${results.data.records.length} result.`)
    return results.data.records
  } catch (e) {
    console.log('lookup failed')
    throw e
  }
}
