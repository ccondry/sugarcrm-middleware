const lookup = require('src/models/lookup')
// set up environment constants
const urlBase = process.env.sugarcrm_url

module.exports = function (page) {
  return async function (req, res, next){
    const field = req.query.field
    const value = req.query.value
    console.log(`looking up SugarCRM ${page} with ${field} = ${value}`)
    if (field && field.length && value && value.length) {
      // we have query params, so look up that customer and return the data
      try {
        // perform lookup
        const url = `${urlBase}/rest/v10/${page}`
        const customers = await lookup(url, field, value)
        // check that we recieved a customer in the results
        switch (customers.length) {
          case 0: {
            // customer record not found - send 404
            return res.status(404).send()
          }
          default: {
            // found 1 or more customers - just pick the first one
            const customer = customers[0]
            // extract customer ID
            const id = customer.id
            console.log(`SugarCRM ${page} ID = ${id}`)
            // construct redirect location
            const location = `${urlBase}/#${page}/${id}`
            // set location header for redirect
            res.set('location', location)
            // send redirect to sugarCRM customer record page for that customer ID
            return res.status(301).send()
          }
        }
      } catch (error) {
        const status = error.response.status
        console.log(status)
        // error during processing
        switch (status) {
          case 404: return res.status(error.response.status).send(`no results for ${page} search`)
        }

      }
    } else {
      // some query params missing, so send 422
      return res.status(422).send(`'field' and 'value' query parameters are required.`)
    }
  }
}
