const getOauth2Token = require('src/models/oauth').getToken
const request = require('request-promise-native')
// set up environment constants
const urlBase = process.env.sugarcrm_url

const uri = `${urlBase}/rest/v10/Notes`
module.exports = async function (id, name, description) {
  console.log(`adding note to Contact ID ${id} - ${name} - ${description}`)
  // get an OAuth2 access token for the lookup request
  const accessToken = await getOauth2Token()
  const body = {
    parent_id: id,
    contact_id: id,
    name,
    description,
    notes_category_c: 'Collection',
    interaction_type_c: 'Note'
  }
  console.log('adding this note:', body)
  // set OAuth2 token header
  const headers = {
    'oauth-token': accessToken
  }
  var options = {
      method: 'POST',
      headers,
      uri,
      body,
      json: true // Automatically stringifies the body to JSON
  }

  // go post the data
  try {
    const results = await request(options)
    // console.log(results)
    console.log(`successfully added note ${results.id} to Contact ${results.contact_name} (${results.contact_id}).`)
    return results
  } catch (e) {
    console.log('lookup failed')
    throw e
  }
}
