var express = require('express')
var path = require('path')
var favicon = require('serve-favicon')
var logger = require('morgan')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var cors = require('cors')
var app = express()
// require('dotenv').load()
// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(cors())
app.use(logger('dev'))
app.use(bodyParser.json({limit: '1mb'}))
app.use(bodyParser.urlencoded({limit: '1mb', extended: false }))

app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
//app.use(express.static('/uploads/'));

app.use(`/api/v1/sugarcrm/accounts`, require('src/routes/accounts'))
app.use(`/api/v1/sugarcrm/click2call`, require('src/routes/click2call'))
app.use(`/api/v1/sugarcrm/contacts`, require('src/routes/contacts'))
app.use(`/api/v1/sugarcrm/pay`, require('src/routes/pay'))

app.use(`/api/v1/sugarcrm/version`, require('src/routes/version'))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  })
})
// console.log('routes: ', app._router.stack)

module.exports = app;
