# sugarcrm-middleware
A simple Express.js app that looks up a SugarCRM customer with server-side OAuth2 handling.

## Usage
1. Install dependencies with `npm install`
2. Copy .env.example to .env and update the setting values for your environment
3. Start the server with `npm start`

4. perform Contacts/Accounts lookup with URLs like this:
HTTP GET `http://localhost:3002/api/v1/sugarcrm/accounts?field=phone_office&value=5551112222`
HTTP GET `http://localhost:3002/api/v1/sugarcrm/contacts?field=phone_work&value=5551112222`

5. perform payment simulation, and add confirmation to Notes in Contact
uses Contacts lookup based on phone_work to find Contact to add Note to
HTTP POST `http://localhost:3002/api/v1/sugarcrm/pay/5551112222`

6. set up click2call in SugarCRM Accounts/Contacts pages, and point to
`https://your.middleware.com/api/v1/sugarcrm/click2call/`
this will trigger Agent Request API call on SocialMiner to get connect agent and customer on voice

### Install as a Service
Install as a systemd service:
```sh
sudo cp ./systemd.service /lib/systemd/system/sugarcrm-middleware.service
sudo systemctl enable sugarcrm-middleware.service
```

### Start service:
```sh
sudo systemctl start sugarcrm-middleware.service
```

### Watch service logs:
```
journalctl -xef
```
